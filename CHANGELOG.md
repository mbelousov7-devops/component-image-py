# Changelog
Make sure to update this file for each merge into develop, otherwise the build fails.
The build relies on the latest version in this file.
Latest versions must be at the top!

## [0.1.3] - 2023-04-13
 - CMD ["python3", "app.py"]

## [0.1.2] - 2023-04-13
 - update version test

## [0.1.1] - 2023-04-12
 - ENTRYPOINT ["/venv/bin/python3", "app.py"]

## [0.1.0] - 2023-04-07
 - init repo
