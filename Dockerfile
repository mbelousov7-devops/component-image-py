ARG RUNTIME_VERSION="3.8"
ARG CICD_IMAGES_REGISTRY

#FROM $CICD_IMAGES_REGISTRY/python:${RUNTIME_VERSION}-slim AS runtime-image
#FROM $CICD_IMAGES_REGISTRY/python:${RUNTIME_VERSION}-slim AS build-image
FROM public.ecr.aws/docker/library/python:${RUNTIME_VERSION}-slim AS runtime-image
FROM public.ecr.aws/docker/library/python:${RUNTIME_VERSION}-slim AS build-image

RUN apt-get update && \
    apt-get install --no-install-suggests --no-install-recommends --yes gcc && \
    python3 -m venv /venv && \
    /venv/bin/pip install --upgrade pip setuptools wheel

# Build the virtualenv as a separate step: Only re-execute this step when requirements.txt changes
FROM build-image AS build-venv
COPY src/requirements.txt /requirements.txt

#RUN --mount=type=secret,id=netrc,dst=/root/.netrc /venv/bin/pip install --disable-pip-version-check -r /requirements.txt
RUN /venv/bin/pip install --disable-pip-version-check -r /requirements.txt

# Copy the virtualenv into a runtime image
FROM runtime-image

RUN useradd -d /app -m appuser
USER appuser
WORKDIR /app

COPY --from=build-venv --chown=appuser:appuser /venv /venv
ENV PATH="/venv/bin:${PATH}"

# Package the "app"
COPY src/app.py \
     ./

CMD ["python3", "app.py"]

